import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class page extends Abstract {

    public WebElement findXpath(String value) {

        WebElement elem = driver.findElement(By.xpath(value));
        return elem;

    }

    public WebElement findId(String value) {

        WebElement elem = driver.findElement(By.id(value));
        return elem;

    }

    public WebElement findName(String value) {

        WebElement elem = driver.findElement(By.name(value));
        return elem;

    }

    public WebElement findCssSelector(String value) {

        WebElement elem = driver.findElement(By.cssSelector(value));
        return elem;

    }

    public WebElement getText(String value) {

        WebElement text = driver.findElement(By.id(value));
        return text;
    }

}




