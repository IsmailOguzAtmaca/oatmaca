import com.sun.jna.platform.win32.Sspi;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollToEvent;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.support.ui.Wait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;

public class loginPage extends page {

    @Test
    static void titleControl() throws InterruptedException {
        System.out.println(driver.getTitle());
        Thread.sleep(2000);
        Assert.assertEquals(driver.getTitle(), "TechCrunch – Startup and Technology News");

        if (driver.getTitle().contains("TechCrunch – Startup and Technology News"))
            System.out.println("Gelen URL = TechCrunch – Startup and Technology News 'dir ");
        else
            System.out.println("Gelen URL  hatalı = TechCrunch – Startup and Technology News olmalıdır ");


    }

    @Test
    static void videosButonAndNews() throws InterruptedException {

        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/header/div/div[2]/ul[1]/li[5]/a")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/ul/li[2]/a")).click();
        Thread.sleep(3000);
        ((JavascriptExecutor) driver).executeScript("scrollTo(0,1000)");
        Thread.sleep(3000);
        Assert.assertEquals("//*[@id=\"root\"]/div/div/div[3]/div/div[3]/div/article[1]/footer/figure/img", "//*[@id=\"root\"]/div/div/div[3]/div/div[3]/div/article[1]/footer/figure/img");
        Thread.sleep(3000);
        ((JavascriptExecutor) driver).executeScript("scrollTo(0,2000)");
        Thread.sleep(3000);
        Assert.assertEquals("//*[@id=\"root\"]/div/div/div[3]/div/div[3]/div/article[3]/header/div[2]/div/span/span/a", "//*[@id=\"root\"]/div/div/div[3]/div/div[3]/div/article[3]/header/div[2]/div/span/span/a");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div[3]/div/div[3]/div/article[4]/footer/figure/img")).click();
        Thread.sleep(2000);
        String actualTitle = driver.getTitle();
        String expectedTitle = "What it was like to be at SpaceX’s historic Falcon Heavy launch | TechCrunch";
        Assert.assertEquals(expectedTitle, actualTitle);


    }

    @Test
    static void titleRight() {

        driver.getTitle();

        if (driver.getTitle().contains("TechCrunch – Startup and Technology News"))
            System.out.println("Gelen URL = TechCrunch – Startup and Technology News 'dir ");
        else
            System.out.println("Gelen URL  hatalı = TechCrunch – Startup and Technology News olmalıdır ");

        System.out.println(driver.getTitle());
        System.out.println("Sitenin Başlığı" + " " + driver.getTitle() + "' dır");
    }


}





